const config = {
  type: "postgres",
  host: process.env.PG_HOST,
  port: 5432,
  database: process.env.PG_DB,
  username: process.env.PG_USER,
  password: process.env.PG_PASSWORD,
  entities: ["./src/entity/**/*.ts"],
  migrations: ["./src/database/migrations/*.ts"],
  cli: {
    entitiesDir: "./src/entity",
    migrationsDir: "./src/database/migrations",
  },
  logging: false,
  synchronize: true,
};

module.exports = config;
