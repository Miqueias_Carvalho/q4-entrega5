import "reflect-metadata";
import app from "./app";
import { createConnection } from "typeorm";

const PORT = 3000;

createConnection()
  .then(() => {
    console.log("========================");
    console.log("| Database Connected!! |");
    console.log("========================");

    app.listen(PORT, () => {
      console.log("server running at https://localhost:3000");
    });
  })
  .catch((err) => console.log(err));
