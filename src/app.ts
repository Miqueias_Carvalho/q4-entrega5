import express from "express";
import router from "./routes";
import { handleError } from "./utils/error";
import { NextFunction, Request, Response } from "express";
import dotenv from "dotenv";

dotenv.config();

const app = express();
app.use(express.json());
app.use(router);

app.use((err: Error, req: Request, res: Response, next: NextFunction) => {
  handleError(err, res);
});

export default app;
