import { NextFunction, Request, Response } from "express";
import * as yup from "yup";
import { ErrorHandler, handleError } from "../utils/error";

export const validate =
  (schema: yup.AnyObjectSchema) =>
  async (req: Request, res: Response, next: NextFunction) => {
    const body = req.body;

    try {
      await schema.validate(body, { abortEarly: false, stripUnknown: true });
      next();
    } catch (e) {
      next(new ErrorHandler(400, { [(e as any).name]: (e as any).errors }));
    }
  };
