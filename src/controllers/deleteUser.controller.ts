import { Request, Response } from "express";
import DeleteUserService from "../services/deleteUser.service";

export default class DeleteUserController {
  async handle(req: Request, res: Response) {
    const { uuid } = req.params;

    const deleteUserController = new DeleteUserService();

    const message = await deleteUserController.execute(uuid);

    return res.json({ message: message });
  }
}
