import { Request, Response } from "express";
import UpdateUserService from "../services/updateUser.service";

export default class UpdateUserController {
  async handle(req: Request, res: Response) {
    const { uuid } = req.params;
    const { isAdm, ...data } = req.body;

    const updateUserService = new UpdateUserService();

    const user = await updateUserService.execute({ uuid, data });

    return res.status(200).json(user);
  }
}
