import { Request, Response, NextFunction } from "express";
import CreateUserService from "../services/createUser.service";

export default class CreateUserController {
  async handle(req: Request, res: Response, next: NextFunction) {
    try {
      const createUserService = new CreateUserService();

      const user = await createUserService.execute(req.body);

      const { password, ...dataWithoutPassword } = user;

      return res.status(201).json(dataWithoutPassword);
    } catch (error) {
      next(error);
    }
  }
}
