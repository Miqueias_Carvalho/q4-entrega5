import { NextFunction, Response, Request } from "express";
import UserProfileService from "../services/profile.service";

export default class UserProfileController {
  async handle(req: Request, res: Response, next: NextFunction) {
    try {
      const uuid = req.user?.uuid;
      const userProfile = new UserProfileService();
      const user = await userProfile.execute(uuid as string);

      const { password, ...dataWithoutPassword } = user;

      return res.status(200).json(dataWithoutPassword);
    } catch (e) {
      next(e);
    }
  }
}
