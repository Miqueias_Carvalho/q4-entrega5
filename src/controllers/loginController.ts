import { Request, Response, NextFunction } from "express";

import LoginService from "../services/login.service";

export default class LoginController {
  async handle(req: Request, res: Response, next: NextFunction) {
    try {
      const { email, password } = req.body;

      const loginService = new LoginService();
      const token = await loginService.execute(email, password);

      return res.status(200).json({ token: token });
    } catch (error) {
      next(error);
    }
  }
}
