import { Request, Response, NextFunction } from "express";
import ListUserService from "../services/listUsers.service";
import { ErrorHandler } from "../utils/error";

export default class ListUsersController {
  async handle(req: Request, res: Response, next: NextFunction) {
    if (!req.user?.isAdm) {
      return next(new ErrorHandler(401, "Unauthorized"));
    }

    const listUserService = new ListUserService();

    const users = await listUserService.execute();

    return res.status(200).json(users);
  }
}
