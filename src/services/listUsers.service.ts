import { UsersRepository } from "../repositories/user.repository";
import { getCustomRepository } from "typeorm";

export default class ListUserService {
  async execute() {
    const usersRepository = getCustomRepository(UsersRepository);

    const users = usersRepository.find();

    return users;
  }
}
