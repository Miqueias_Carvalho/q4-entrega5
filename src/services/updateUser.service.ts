import { UsersRepository } from "../repositories/user.repository";
import { getCustomRepository } from "typeorm";
import bcrypt from "bcryptjs";

interface Iprops {
  uuid: string;
  data: {
    name?: string;
    password?: string;
    email?: string;
  };
}

export default class UpdateUserService {
  async execute({ uuid, data }: Iprops) {
    const usersRepository = getCustomRepository(UsersRepository);

    if (data.hasOwnProperty("password")) {
      data.password = bcrypt.hashSync(data.password as string, 10);
    }

    await usersRepository.update(uuid, data);

    const user = usersRepository.findOne({ uuid });

    return user;
  }
}
