import { UsersRepository } from "../repositories/user.repository";
import { getCustomRepository } from "typeorm";
import { ErrorHandler } from "../utils/error";

interface IUserRequest {
  name: string;
  email: string;
  password: string;
  isAdm: boolean;
}

export default class CreateUserService {
  async execute({ name, email, password, isAdm }: IUserRequest) {
    const usersRepositor = getCustomRepository(UsersRepository);

    const emailAlreadyExists = await usersRepositor.findOne({ email });

    if (emailAlreadyExists) {
      throw new ErrorHandler(400, "E-mail already registered");
    }

    const user = usersRepositor.create({ name, email, isAdm, password });

    await usersRepositor.save(user);

    return user;
  }
}
