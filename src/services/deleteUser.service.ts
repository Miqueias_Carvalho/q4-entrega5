import { UsersRepository } from "../repositories/user.repository";
import { getCustomRepository } from "typeorm";

export default class DeleteUserService {
  async execute(uuid: string) {
    const usersRepository = getCustomRepository(UsersRepository);

    await usersRepository.delete(uuid);

    return "User deleted with success";
  }
}
