import { UsersRepository } from "../repositories/user.repository";
import { getCustomRepository } from "typeorm";
import { ErrorHandler } from "../utils/error";

export default class UserProfileService {
  async execute(uuid: string) {
    const userRepository = getCustomRepository(UsersRepository);

    const user = await userRepository.findOne({ uuid });

    if (!user) {
      throw new ErrorHandler(404, "User not found");
    }

    return user;
  }
}
