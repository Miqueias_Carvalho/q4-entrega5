import { Router } from "express";

import { UserSchema } from "./schemas/userSchemas";
import { LoginSchema } from "./schemas/loginSchema";
import { UpdateSchema } from "./schemas/updateSchema";

import { validate } from "./middlewares/validation.middleware";
import { isAuthenticated } from "./middlewares/authentication.middleware";
import { isAdmUser } from "./middlewares/adm.middleware";
import { isAuthorizated } from "./middlewares/authorization.middleware";

import CreateUserController from "./controllers/createUser.controller";
import ListUsersController from "./controllers/listUsers.controller";
import DeleteUserController from "./controllers/deleteUser.controller";
import UpdateUserController from "./controllers/updateUser.controller";
import LoginController from "./controllers/loginController";
import UserProfileController from "./controllers/profile.controller";

const router = Router();

const createUser = new CreateUserController();
const listUsers = new ListUsersController();
const deleteUser = new DeleteUserController();
const updateUse = new UpdateUserController();
const login = new LoginController();
const profile = new UserProfileController();

router.post("/users", validate(UserSchema), createUser.handle);
router.post("/login", validate(LoginSchema), login.handle);
router.get("/users", isAuthenticated, isAdmUser, listUsers.handle);
router.get("/users/profile", isAuthenticated, profile.handle);
router.patch(
  "/users/:uuid",
  isAuthenticated,
  isAdmUser,
  isAuthorizated,
  validate(UpdateSchema),
  updateUse.handle
);
router.delete(
  "/users/:uuid",
  isAuthenticated,
  isAdmUser,
  isAuthorizated,
  deleteUser.handle
);
export default router;
