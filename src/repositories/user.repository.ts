import { EntityRepository, Repository } from "typeorm";

import { User } from "../entity/user.entity";

@EntityRepository(User)
class UsersRepository extends Repository<User> {}

export { UsersRepository };
