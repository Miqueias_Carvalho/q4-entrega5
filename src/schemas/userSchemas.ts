import * as yup from "yup";

export const UserSchema = yup.object().shape({
  name: yup.string().required(),
  email: yup.string().email().required(),
  isAdm: yup.boolean().required(),
  password: yup.string().min(4).required(),
});
